// var a ="hello world";
// var tableau =["h","e","l","l","o"," ","w","o","r","l","d"]; //c'est le même que : var a ="hello world";
// console.log(a);
// console.log(tableau);

//BOUCLES FOR

// for (let i = 0; i < tableau.length; i++) {
//    console.log("index courant : " + i + " valeur de la case courante : " + tableau[i]);  
// } 
/** On va exécuter le code tant que i est inférieur à la longueur du tableau, 
si cela se confirme, que c'est VRAI, on va afficher dans la console la valeur actuelle de i, dans ce cas ça sera H
A chaque fois i est incrémenté de 1, donc après i=1 ben il affichera la valeur E et ainsi de suite, jusqu'à arriver a i=10 qu'il affichera la valeur de D
et après la boucle s'arretera car il aura arrivé à la longueur maximale, car la longueur est de 11 et i va de 0 à 10, donc 11 espaces*/

//BOUCLES WHILE

// var i=0;
// while (i < tableau.length) {
//     console.log("index courant : " + i + " valeur de la case courante : " + tableau[i] + " et c'est le " + (i+1) + " ième element du tableau");
//     i++;
//     //console.log("index courant : " +i+ " valeur de la case courante : " + tableau[i++]); 
// } 
/** J'initialise une varible i =0
La boucle dit, tant que ça retourne VRAI tu m'exécutes le bloc, donc dans notre cas
tant que i est inférieur à la longueur du tableau, tu m'affiches la valeur de i et après tu incrémentes i de 1, à la fin de la boucle tu va retester
et pour savoir s'il doit refaire la boucle, il va vérifier que la condition est VRAI, est-ce que 1 est inférieur à la longueur du tableau? 
si c'est VRAI il re-exécute le code et ainsi de suite*/

//Exercice 1. Inverser l'ordre des charactères du tableau exo -> d l r o w   o l l e h
// var exo =["h","e","l","l","o"," ","w","o","r","l","d"];

// for (let i = 10; i >= 0; i--) { //tant que i est égal ou supérieur à 0 tu fais le traitement
//     console.log("index courant : " + i + " valeur de la case courante : " + exo[i]);    
// }

// var i=10; //index, où se balader dans le tableau exo. On va initialiser à 10, au dernier élement de la table, la lettre D
// while (i>=0) { //tant qu'on n'arrive pas à i=0 au premier élement, on n'arrête pas la boucle, donc i doit être égal ou supérieur à 0 afin que la boucle puisse tourner
//                 //puisqu'on commence à i=10 et ça va décrementer jusqu'à 0
//     console.log(exo[i]);  
//     i--; //à chaque tour de boucle on veut décrementer i : 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 et 0
// }

/**
 * Exercice 2  sur les suites
 * 1- Afficher la suite des nombres entiers (0>1000) afficher la prochaine valeur du nombre entier
 * 2- Afficher la suite de fibonacci (nombre de valeurs à afficher : 50) * 
 * 3- Afficher les 50 premiers nombres entiers pairs
 *          - puis impairs
 * Extra. les nb pairs de la suite de fibonacci
 * Extra. les nb impairs de la suite de fibonacci
 * Le prochain nombre dépend du nb précedent
 */

//1. Afficher la suite des nb entiers de 0 à 1000


// for (let i = 0; i <= 1000; i++) {
//     console.log (i);
// }

//2. Afficher la suite de fibonacci (50 premiers nombres)


// var fib =[]; //on crée notre table où l'on va stocker les informations
// fib[0]=0; //on ajoute un premier élement qui sera = 0 situé à l'espace 0, car F0=0
// fib[1]=1; //on ajoute un deuxième élement qui sera = 1 situé à l'espace 1, car F1=1 et après on pourra commencer la suite sans problème, puisqu'ils n'ont pas de n-2 et n-1 à la fois
// console.log(fib[0]);
// console.log(fib[1]);
// for (let i = 2; i < 50; i++) { //on initialise à i=2, donc la 2ème position et tant que i est inférieur ou égal à 50, on fait tourner la boucle et on incrémente i  de 1
//     fib[i] = fib[i-1] + fib[i-2]; //on stocke la valeur dans notre table, cette valeur prendra en compte l'opération de fibonacci suivante Fn = Fn-1 + Fn-2
//     console.log(fib[i]);//on affiche la valeur obtenu dans chaque calcul
// }

//Méthode Alexandre, plus pratique
// var a = 0;
// var b = 1;
// for (i = 0; i < i<50; i++) {
//     console.log(a);
//     b = b+a;
//     a = b-a; 
// }


// //Méthode Loic
// var n_1 = 0;
// var n_2 = 1;
// var n = n_1 + n_2;
// var OccurrencesRestantes = 50; // ça va me permettre de conditionner ma boucle, tant que j'ai des occurrences restantes, la boucle tournera
// //On affiche les 3 premières valeurs, car elles ne sortiront pas dans la boucle de base par le calcul
// console.log(n_1);
// OccurrencesRestantes--; //qui est égal à OccurrencesRestantes = OccurrencesRestantes -1; à chaque console.log on réduit de 1, comme dans la boucle
// console.log(n_2);
// OccurrencesRestantes--; //ainsi on respecte les 50 premières chiffres
// console.log(n);
// OccurrencesRestantes--; //du coup si demain on veut changer le nb de chiffres à afficher, ben il suffira de changer la var OccurrencesRestantes, sans devoir à modifier tout
// while (OccurrencesRestantes >= 0) {
//     //traitement pour l'affichage  des nombres de fibo
//     n_1 = n_2;
//     n_2 = n;
//     n = n_1 + n_2;
//     //affichage des nombres
//     console.log(n);
//     //on décremente le nb d'occurrences restantes, on réduit de 1 à chaque fois
//     OccurrencesRestantes--;
// }

//3. 50 premiers nombres pairs

// var pair=[]; //je crée une table où stocker mes valeurs
// for (let i = 0; i <= 100 ; i++) { //on initalise à 0 et tant que i est égal ou inférieur à 100 tu fais le calcul et après incrémente i de 1. J'ai fais en base de 100 maxi, car pour avoir les 50 premiers pairs, j'ai besoin d'aller de 0 à 100 : 0 , 2, ..., 100
//     pair.push(i%2==0) //je stocke la valeur dans la table tenant compte que ça sera true pour ceux qui sont pairs, donc son modulo de 2 est = 0
//     if (pair[i] === true){ //si la valeur de i=true, donc pair, tu m'affiches le résultat
//         console.log (i + " est pair");
//     }
// }

// //Méthode Loic
// var nombresRestantsAAfficher = 50;
// var nombreAAfficher = 0;
// //avant de rentrer dans la boucle, s'assurer que le nombre à afficher est pair
// if (nombreAAfficher %2 == 0){
//     //la boucle nous permets d'afficher les nombres pairs, si à la première itération le nb est pair
//     while (nombresRestantsAAfficher >= 0){
//         //tant qu'il y a des nombres à afficher, on affiche le nombre
//         console.log(nombreAAfficher);
//         //calculer le prochain nombre à afficher
//         nombreAAfficher = nombreAAfficher + 2; //nombreAAfficher += 2;
//         //je décremente le nombre restant à afficher
//         nombresRestantsAAfficher--;
//     }
// }

// Façon simplifiée, faite après fibonacci, ça m'a donné l'idée après
// for (let i = 0; i <= 100 ; i++) { 
//     if ([i]%2 === 0){ //si la valeur de i%2=0, donc pair, tu m'affiches l'index qui fait référence à cette condition
//         console.log (i + " est pair");
//     }
// }

//3. 50 premiers nombres impairs

// var impair=[];
// for (let i = 0; i <= 100 ; i++) {
//     impair.push(i%2== 0)
//     if (impair[i] === false){ //si la valeur de i=false, donc impair, tu m'affiches le résultat
//         console.log (i + " est impair");
//     }
// }

// //Méthode Loic
// var compteur = 50;
// var nombre = 0;
// while(compteur >= 0){
//     //s'assurer que le nb est impair avant de l'afficher
//     if (nombre %2 == 1) {
//         console.log(nombre);
//         compteur --;
//     }
//     //calcul du prochain nombre de la suite des nb entiers
//     nombre ++;
// }

// Façon simplifiée, faite après fibonacci, ça m'a donné l'idée après
// for (let i = 0; i <= 100 ; i++) { 
//     if ([i]%2 != 0){ //si la valeur de de i%2 est différente de 0, donc impair, tu m'affiches l'index qui fait référence à cette condition
//         console.log (i + " est impair");
//     }
// }

//Extra. nombres pairs des 50 premiers nombres de fibonacci - COMMENT PRENDRE EN COMPTE LE 0 ET 1 DU DEBUT??

// var fib =[]; 
// fib[0]=0; 
// fib[1]=1; 
// for (let i = 2; i < 50; i++) { 
//     fib[i] = fib[i-1] + fib[i-2];
//     if (fib[i]%2 === 0 ){ //si le modulo de la valeur qu'on trouve dans la position i (fib[i]) est 0, donc pair, on affiche 
//         console.log(fib[i]); 
//     }
// }

//Extra. nombres impairs des 50 premiers nombres de fibonacci - COMMENT PRENDRE EN COMPTE LE 0 ET 1 DU DEBUT??

// var fib =[]; 
// fib[0]=0; 
// fib[1]=1; 
// for (let i = 2; i < 50; i++) { 
//     fib[i] = fib[i-1] + fib[i-2];
//     if (fib[i]%2 != 0 ){ //si le modulo de la valeur qu'on trouve dans la position i (fib[i]) est différent de 0, donc impair, on affiche
//         console.log(fib[i]); 
//     }
// }

/** 
 * Exercice 3 sur les palindromes
 * 
 * Vérifier si le mot est un palidrome ou pas
 
//Comparer premiere lettre avec dernière, et ainsi ensuite: kayak (01234) avec kayak (à l'envers, depuis la fin 43210) -> 0 avec 4, 1 avec 3, 2 avec 2, 3 avec 1, 4 avec 0

var ordre=["kayak","parole","ono"]; //création liste pour intégrer les mots
var desordre=[]; //création d'une autre liste pour intégrer les mots qu'on va inverser
for (let i = 0; i < ordre.length; i++) { //on initialise par 0 et tant que i est inférieur à la longueur de la table, on fait l'opération
    desordre[i]=ordre[i.split("").reverse().join("")];//prendre le mot de la liste et l'inverser : on sépare le mot lettre par lettre, on inverse son ordre et on join toutes les lettres 
    //création d'une autre liste où stocker ce nouveau mot? et ainsi comparer les deux listes, tenant compte son index???
    //comparer le mot de la liste avec celui qu'on a inversé
    if (desordre[i] === odre[i]){ //condition pour l'afficher dans la console, que les mots des deux listes soient les mêmes
        console.log ("ton mot est un palindrome!");
    } else {
        console.log("ton mot n'est pas un palidrome!")
    }
}
*/
//Méthode classe, pas fini
// var a ="kayak";
// var i = 0;
// var j= a.length -1;
// while (j>0 /** ou bien a >= a.length*/) {
//     //vérirfier qu'on compare les mêmes lettres
//     if (a[i] === a[j]) {
//         console.log("ce sont les mêmes lettres");
//     }
//     //à chaque fois tu va incrémenter i et décrementer j
//     i++;
//     j--;
// }


var mot ="kayak";
//nos index : on veut tester notre premier et dernier caractère à l'aide des index, comparer  i=0 avec j=4, donc k=k
var i = 0; //index à incrémenter
var j= mot.length -1; //index à décrementer
//Postulat. On considére que notre mot est un palidrome par défaut et arrêter notre traitement dès qu'il ne l'est pas
var isPalindrome = true;
//Traitement.
while (isPalindrome) {
    if (mot[i] === mot[j]) { //si les deux charactères sont les mêmes, on incrémente i et on décremente j. On valide le postulat du départ, donc on continue dans le boucle
        i++;
        j--;  
    } else {
        isPalindrome = false; //ça sortira de la boucle si les lettres ne sont pas les mêmes. On ne valide pas le postulat de départ, donc on sort
    }
    //vérifier qu'on a passé tout le mot, est-ce qu'on à tester toutes les lettres
    if (j == 0) {
        break;
     } //break permet de sortir de la boucle 
}
//Résultat.
if (isPalindrome) {
    console.log("le mot " + mot + " est un palindrome");
}else{
    console.log("le mot " + mot + " n'est un palindrome");
}